from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('daftar-kegiatan/', views.daftar, name="daftar"),
    path('tambah-kegiatan/', views.tambah, name="tambah"),
    path('tambah-peserta/', views.tambah_peserta, name='karepmu'),
]
